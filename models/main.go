package models

import (
	"github.com/jinzhu/gorm"
)

func Init() *gorm.DB {

	db, err := gorm.Open("postgres", "host=127.0.0.1 port=5432 user=change dbname=change password=change.2020 sslmode=disable")
	if err != nil {
		panic("failed to connect database" + err.Error())
	}
	//defer db.Close()

	db.AutoMigrate(&User{})

	return db

}
