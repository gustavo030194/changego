package server

import (
	"changego/config"
	"changego/controllers"
	"changego/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB

func Init() {

	//Init DB
	db = models.Init()

	//Inyect services to controllers
	controllers := new(controllers.Controller)
	controllers.DB = db

	//Configure routes
	r := SetupRouter(controllers)
	r.Run(config.GetPort())
}
