package server

import (
	"changego/controllers"
	"changego/docs"
	"net/http"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"   // gin-swagger middleware
	"github.com/swaggo/gin-swagger/swaggerFiles" // swagger embed files
)

func SetupRouter(c *controllers.Controller) *gin.Engine {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	//Init Controllers
	userController := new(controllers.UserController)
	userController.Controller = c

	//Routes for userRouter of api server
	userRouterGroup := router.Group("user")
	{
		userRouterGroup.GET("/list", userController.List)
		userRouterGroup.POST("/", userController.Create)
	}

	//Routes for swagger
	swagger := router.Group("swagger")
	{ // programatically set swagger info
		docs.SwaggerInfo.Title = "Golang REST API Starter"
		docs.SwaggerInfo.Description = "This is a sample backend written in Go."
		docs.SwaggerInfo.Version = "1.0"
		// docs.SwaggerInfo.Host = "cloudfactory.swagger.io"
		// docs.SwaggerInfo.BasePath = "/v1"

		swagger.GET("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

	router.NoRoute(func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/swagger/index.html")
	})

	return router

}
