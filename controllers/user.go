package controllers

import (
	"changego/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserController struct {
	*Controller
}

// Status godoc
// @Summary Responds with 200 if service is running
// @Description Health check for service
// @Produce  json
// @Success 200 {string} string "Working!"
// @Router /user/list [get]
func (u UserController) List(c *gin.Context) {

	users := u.DB.Find(new([]models.User))
	if users.Error != nil {
		c.JSON(http.StatusBadRequest, "Error al consultar registros!")
	}
	c.JSON(http.StatusOK, users.Value)
}

// Status godoc
// @Summary Responds with 200 if service is running
// @Description Health check for service
// @Produce  json
// @Success 200 {string} string "Working!"
// @Router /user [post]
func (u UserController) Create(c *gin.Context) {
	bodyParse := new(models.User)
	c.ShouldBindJSON(bodyParse)

	fmt.Println(bodyParse)
	result := u.DB.Create(bodyParse)
	c.JSON(http.StatusOK, result)
}
