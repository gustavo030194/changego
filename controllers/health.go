package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type HealthController struct{}

// Status godoc
// @Summary Responds with 200 if service is running
// @Description Health check for service
// @Produce  json
// @Success 200 {string} string "Working!"
// @Router /health/health [get]
func (h HealthController) Status(c *gin.Context) {
	c.String(http.StatusOK, "Working!")
}
