module changego

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/gin-gonic/gin v1.6.3
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/stretchr/testify v1.5.1
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.6.5
	golang.org/x/net v0.0.0-20200506145744-7e3656a0809f // indirect
	golang.org/x/tools v0.0.0-20200508205758-2d0106b2df1a // indirect
)
